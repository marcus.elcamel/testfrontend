import Modal from 'components/Modal/Modal';
import { ColourInterface, PeopleInterface } from 'interfaces/interfaces';
import React, { useState } from 'react';
import { editPerson, getPerson } from 'services/people';
import './EditForm.scss';

interface EditFormProps {
  onClick: () => void;
  personToEdit: PeopleInterface;
}

const EditForm: React.FC<EditFormProps> = props => {
  const [person, setPerson] = useState<PeopleInterface>(props.personToEdit);
  const [isAuthorised, setIsAuthorised] = useState(props.personToEdit.authorised);
  const [isEnabled, setIsEnabled] = useState(props.personToEdit.enabled);
  const [colours, setColours] = useState<ColourInterface[]>([]);

  const saveChanges = async (e: React.MouseEvent<HTMLButtonElement>) => {
    e.preventDefault();

    try {
      person.colours = colours;
      person.authorised = isAuthorised;
      person.enabled = isEnabled;

      await editPerson(person).then(async person => {
        await getPerson(person.id).then(person => {
          setIsAuthorised(isAuthorised);
          setPerson(person);
        });
      });
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <Modal onClose={props.onClick}>
      <form>
        <label>First name: </label> <br />
        <input
          type="text"
          name="fname"
          defaultValue={props.personToEdit.firstName}
          onChange={e => setPerson({ ...person, firstName: e.target.value })}
        />{' '}
        <br />
        <br />
        <label>Last name: </label> <br />
        <input
          type="text"
          name="lname"
          defaultValue={props.personToEdit.lastName}
          onChange={e => setPerson({ ...person, lastName: e.target.value })}
        />{' '}
        <br />
        <br />
        <label>Authorisation: </label> <br />
        <input
          type={'checkbox'}
          name="auth"
          id="auth"
          onChange={() => setIsAuthorised(!isAuthorised)}
          checked={isAuthorised}
        />
        <br />
        <br />
        <label>Enabled: </label> <br />
        <input
          type={'checkbox'}
          name="enable"
          id="enable"
          onChange={() => setIsEnabled(!isEnabled)}
          checked={isEnabled}
        />
        <br />
        <br />
        <label>Favourite colour(s): </label> <br />
        <input
          type="checkbox"
          id="red"
          value="Red"
          onChange={e => {
            if (e.target.checked) {
              setColours([
                ...colours,
                { id: 1, name: e.target.value }
              ]);
            } else {
              setColours(
                colours.filter(colour => colour.name !== e.target.value)
              );
            }
          }}
        />
        <label htmlFor="red">Red</label>
        <input
          type="checkbox"
          id="green"
          value="Green"
          onChange={e => {
            if (e.target.checked) {
              setColours([
                ...colours,
                { id: 2, name: e.target.value }
              ]);
            } else {
              setColours(
                colours.filter(colour => colour.name !== e.target.value)
              );
            }
          }}
        />
        <label htmlFor="green">Green</label>
        <input
          type="checkbox"
          id="blue"
          value="Blue"
          onChange={e => {
            if (e.target.checked) {
              setColours([
                ...colours,
                { id: 3, name: e.target.value }
              ]);
            } else {
              setColours(
                colours.filter(colour => colour.name !== e.target.value)
              );
            }
          }}
        />
        <label htmlFor="blue">Blue</label>
        <br />
        <br />
        <div className='buttons'>
          <button onClick={saveChanges}>Submit</button>
          <button onClick={props.onClick}>Close</button>
        </div>
      </form>
    </Modal>
  );
};

export default EditForm;
