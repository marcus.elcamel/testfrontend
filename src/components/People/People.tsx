import Card from 'components/Card/Card';
import { PeopleInterface } from 'interfaces/interfaces';
import React, { useEffect, useState } from 'react';
import { getPeople } from '../../services/people';
import Person from './Person';
import './People.scss';

const People: React.FC = () => {
  const [people, setPeople] = useState<PeopleInterface[]>([]);

  const fetchData = async (): Promise<void> => {
    try {
      const result: Array<PeopleInterface> = await getPeople();

      result.sort(
        (a, b) =>
          a.firstName.localeCompare(b.firstName) -
          b.firstName.localeCompare(a.firstName)
      );

      result.forEach(person => {
        person.colours.sort((a, b) => a.name.localeCompare(b.name));
      });

      setPeople(result);
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    fetchData();
  }, [people]);

  return (
    <section>
      {people?.length === 0 ? (
        <p>There are no people in the database.</p>
      ) : (
        <div className="flex">
          {people?.map(person => (
            <Card key={person.id}>
              <Person person={person} />
            </Card>
          ))}
        </div>
      )}
    </section>
  );
};

export default People;
