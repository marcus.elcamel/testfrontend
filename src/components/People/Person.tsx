import EditForm from 'components/EditForm/EditForm';
import { PeopleInterface } from 'interfaces/interfaces';
import React, { useState } from 'react';

interface PersonProps {
  person: PeopleInterface;
}

const Person: React.FC<PersonProps> = ({ person }: PersonProps) => {
  const [editFormIsShown, setEditFormIsShown] = useState(false);
  const [fullName, setFullName] = useState(
    `${person.firstName} ${person.lastName}`
  );

  const toggleModal = () => {
    setEditFormIsShown(!editFormIsShown);
  };

  const isPalindrome = (str: string) => {
    const reversed = str
      .split('')
      .reverse()
      .join('');
    return str === reversed;
  };

  const styles = {
    authorised: {
      color: person.authorised ? 'green' : 'red'
    },

    enabled: {
      color: person.enabled ? 'green' : 'red'
    },

    palindrome: {
      color: isPalindrome(fullName.toLowerCase()) ? 'green' : 'red'
    }
  };

  return (
    <div>
      {editFormIsShown ? (
        <EditForm onClick={toggleModal} personToEdit={person} />
      ) : null}
      <div>
        <h3>
          Name: {person.firstName} {person.lastName}
        </h3>
        <label>Authorised: </label>
        <span style={styles.authorised}>
          {person.authorised ? 'Yes' : 'No'}
        </span>
        <br /> <br />
        <label>Enabled: </label>
        <span style={styles.enabled}>{person.enabled ? 'Yes' : 'No'}</span>
        <p>
          Favourite colour(s):{' '}
          {person.colours?.map(colour => colour.name).join(', ')}
        </p>
        <label>Palindrome: </label>
        <span style={styles.palindrome}>
          {isPalindrome(fullName.toLowerCase()) ? 'Yes' : 'No'}
        </span>
        <br /> <br />
        <button onClick={toggleModal}>Edit</button>
      </div>
    </div>
  );
};

export default Person;
