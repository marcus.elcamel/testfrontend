import React, { createContext } from 'react';
import { PeopleInterface } from 'interfaces/interfaces';

interface PeopleContextInterface {
  people?: PeopleInterface
  setPeople: (item: PeopleInterface) => void;
}

const PeopleContext = createContext<PeopleContextInterface>({
  people: undefined,
  setPeople: (person: PeopleInterface) => null,
});

export default PeopleContext;
