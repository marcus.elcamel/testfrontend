import { PeopleInterface } from 'interfaces/interfaces';
import React, { useState } from 'react';
import PeopleContext from './PeopleContext';

const PeopleProvider: React.FC = props => {
  const [people, setPeople] = useState<PeopleInterface>();

  return (
    <PeopleContext.Provider value={{people, setPeople}}>
      {props.children}
    </PeopleContext.Provider>
  );
};

export default PeopleProvider;
