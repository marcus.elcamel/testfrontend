export interface ColourInterface {
  id: number;
  name: string;
}

export interface PeopleInterface {
  id: number;
  firstName: string;
  lastName: string;
  authorised: boolean;
  enabled: boolean;
  colours: Array<ColourInterface>;
}

export interface ButtonInterface {
  onClick: () => void;
}
