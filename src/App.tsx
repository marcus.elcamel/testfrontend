import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Layout from 'components/Layout/Layout';
import PeopleProvider from 'store/People/PeopleProvider';
import People from 'components/People/People';

const App: React.FC = () => {
  return (
    <React.Fragment>
      <Router>
        <Layout>
          <Switch>
            <PeopleProvider>
              <Route exact path="/" component={People}></Route>
            </PeopleProvider>
          </Switch>
        </Layout>
      </Router>
    </React.Fragment>
  );
};

export default App;
