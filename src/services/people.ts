import { PeopleInterface } from 'interfaces/interfaces';
import { get, put } from 'utilities/http-client';

export async function getPeople(): Promise<Array<PeopleInterface>> {
  try {
    const response = await get('https://answer-colours-api.azurewebsites.net/api/people');

    if (!response.ok) {
      return Promise.reject();
    }

    const result: Array<PeopleInterface> = [...(await response.json())];
    return result;

  } catch (error) {
    console.log(error);
    return Promise.reject();
  }
}

export async function getPerson(id :number): Promise<PeopleInterface> {
  try {
    const response = await get(`https://answer-colours-api.azurewebsites.net/api/people/${id}`);

    if (!response.ok) {
      return Promise.reject();
    }

    const result: PeopleInterface = await response.json();
    return result;

  } catch (error) {
    console.log(error);
    return Promise.reject();
  }
}

export async function editPerson(person: PeopleInterface): Promise<PeopleInterface> {
  try {
    const response = await put(`https://answer-colours-api.azurewebsites.net/api/people/${person.id}`, person);

    if (!response.ok) {
      return Promise.reject();
    }

    const result: PeopleInterface = await response.json();
    return result;

  } catch (error) {
    console.log(error);
    return Promise.reject();
  }
}

