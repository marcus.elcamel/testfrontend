import { ColourInterface } from 'interfaces/interfaces';
import { get } from 'utilities/http-client';

export async function getColours(): Promise<Array<ColourInterface>> {
  try {
    const response = await get('https://answer-colours-api.azurewebsites.net/api/colours');

    if (!response.ok) {
      return Promise.reject();
    }

    const result: Array<ColourInterface> = [...(await response.json())];
    return result;

  } catch (error) {
    console.log(error);
    return Promise.reject();
  }
}
